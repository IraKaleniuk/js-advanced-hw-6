"use strict";
const root = document.querySelector("#root");

class IPService {
    constructor(target) {
        this.target = target;
    }
    static async #getIP() {
        try {
            let response = await fetch("https://api.ipify.org/?format=json");
            return await response.json();
        } catch (error) {
            alert(error.message);
        }
    }

    static async #getInfo(query) {
        try {
            let response = await fetch(`http://ip-api.com/json/${query}?fields=status,message,continent,country,regionName,city,district`);
            return await response.json();
        } catch (error) {
            alert(error.message);
        }
    }

    static #createElement(tagName, classList = [], props = {}) {
        const element = document.createElement(tagName);
        element.classList.add(...classList);

        Object.entries(props).forEach(([key, value]) => element[key] = value);

        return element;
    }

    async find() {
        const listInDocument = document.querySelector(".list");
        if (listInDocument) listInDocument.remove();

        const loader = document.querySelector(".loader");
        loader.style.display = "inline-block";

        const IPObj = await IPService.#getIP();

        const {continent, country, regionName: region, city, district} = await IPService.#getInfo(IPObj.ip);
        loader.style.display = "none";

        const list = IPService.#createElement("ul", ["list"]);

        const listItems = Object.entries({continent, country, region, city, district})
            .map(([key, value]) => {
                const li = IPService.#createElement("li", value === "" ? ["listItem", "notAvailable"] : ["listItem"]);
                li.innerHTML = `<h5  class="listItemKey">${key}: </h5><span class="listItemValue">${value || "notAvailable"}</span>`;

                return li;
            })

        list.append(...listItems);
        this.target.append(list);
    }
}


const findBtn = document.querySelector("#findBtn");
findBtn.addEventListener("click", e => {
    const ip = new IPService(root);
    ip.find();
})